# Breakthrough Solutions - A-Level

A set of solutions to possible breakthrough challenges for 2022 computer science A-Level (AQA)

## Project structure
```
  - /
    - challenges/ # (Actual solutions)
    - original/ # (Original files)
    - sample-questions.pdf # (List of tasks)
```

## Programming conventions
For each challenge, the general style of coding has been followed when implementing a solution. For example, the sample program source code opts to use PascalCase for variable and function names, as well as prefacing __PrivateMethods with `__` and prefacing _PrivateVariableNames with `_`.

Sections where code has been edited are marked with `### Start edit` and `### End edit` before and after respectively. Original program code that's no longer used in the various solutions has been *\# commented out*.